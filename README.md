### What is this repository for? ###

* Content Analytics for the Integrated System

### How do I get set up? ###

1. Install Integrated
2. Fork this repository into your Integrated installation.

### Contribution guidelines ###

* Feel free to add anything.