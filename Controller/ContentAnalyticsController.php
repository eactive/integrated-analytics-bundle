<?php
/**
 * Authors: Peter & Michael
 * Date: 4/15/14
 * Time: 11:30 AM
 */
namespace Stage\StatBundle\Controller;

use Integrated\Bundle\ContentBundle\Document\Content\Taxonomy;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ContentAnalyticsController extends Controller
{
    /**
     * @var string
     */
    protected  $articleClass = 'Integrated\\\Bundle\\\ContentBundle\\\Document\\\Content\\\Article';

    /**
     * Show index page
     *
     * @Template()
     * @return array
     */
    public function indexAction()
    {
//         TODO: Remove
//        $this->clrSolr();
        /* @var $dm \Doctrine\ODM\MongoDB\DocumentManager */
//        $dm = $this->get('doctrine_mongodb')->getManager();
//        $documents = $dm->getRepository($this->articleClass)->findAll();
//
//        foreach($documents as $doc)
//        {
//            echo 'voor: <br>';
//            echo $doc->getCreatedAtHax();
//            echo "<br> -->";
//            die();
//        }

        return array();
    }

    /**
     * Return contenttypes and taxonomy items for the homepage in JSON format
     *
     * @Template()
     * @param Request $request
     * @return JsonResponse
     */
    public function jsonAction(Request $request)
    {
        $dateStart = $request->get('sd');
        $dateStop = $request->get('ed');
        if(($dateStart || $dateStop) != null)
        {
            if(($this->validateDate($dateStart)) && ($this->validateDate($dateStop)))
            {
                $c = $this->getContentTypes($dateStart, $dateStop);
                $t = $this->getTaxItems($dateStart, $dateStop);

                if((count($c) == 0) && (count($t)) == 0)
                {
                    $data[] = array('result' => 'No results');
                }
                else
                {
                    $data[] = array(
                        'result' => 'OK',
                        'contenttypes' => $c,
                        'taxonomies' => $t);
                }
            }
            else
            {
                $data[] = Array('result' => 'Dateformat is not valid.');
            }
        }
        else
        {
            $data[] = Array('result' => 'No date specified');
        }
        return new JsonResponse($data);
    }

    /**
     * Show detailpage
     *
     * @Template()
     * @param Request $request
     * @return array
     */
    public function detailAction(Request $request)
    {
        $request->getPathInfo();
        $type = $request->get('ctype');
        $taxtype = '';
        // Check whether we need to get taxonomy or contenttype items.
        if($request->get('taxonomyItem'))
        {
            $title = $request->get('taxonomyItem');
            $taxtype = $request->get('taxonomyType');
        }
        else
        {
            $title = $request->get('contentType');
        }
        // Create title for display in table.
        ($type == 'contenttype') ? $typenaam = 'Content Types' : (($type == 'taxonomy') ? $typenaam = $taxtype : $typenaam = $type);

        // Create 'fancy' title for display while keeping slug for references.
        $slug = $title;
        $title = ucfirst(str_replace('_', ' ', $title));

        return array(
            'type' => $type,
            'typenaam' => str_replace('facet_','',$typenaam),
            'title' => $title,
            'slug' => $slug
        );
    }

    /**
     * Return contenttypes and taxonomy items for the detail page in JSON format
     *
     * @Template()
     * @param Request $request
     * @return JsonResponse
     */
    public function detailjsonAction(Request $request)
    {
        $data = [];
        $dateStart = $request->get('sd');
        $dateStop = $request->get('ed');
        $request->getPathInfo();
        $type = $request->get('ctype');
        $ctype = $request->get('contentType');
        $taxtype = $request->get('taxonomyType');
        $taxitem = $request->get('taxonomyItem');
        if(($dateStart && $dateStop) != null)
        {
            if($type == 'contenttype' || $type == 'taxonomy')
            {
                if($ctype || $taxtype)
                {
                    if($type == 'taxonomy')
                    {
                        $ct = $this->getContentTypeByName($dateStart, $dateStop, $taxtype, $taxitem);
                    }
                    elseif($type == 'contenttype')
                    {
                        $ct = $this->getTaxItems($dateStart, $dateStop, $ctype);
                    }
                    else
                    {
                        $data[] = array('result' => 'Invalid input!');
                    }

                    if(isset($ct))
                    {
                        if($type == 'taxonomy')
                        {
                            $ct = $this->getContentTypeByName($dateStart, $dateStop, $taxtype, $taxitem);
                        }
                        elseif($type == 'contenttype')
                        {
                            $ct = $this->getTaxItems($dateStart, $dateStop, $ctype);
                        }
                        else
                        {
                            $data[] = array('result' => 'Invalid input!');
                        }

                        if(isset($ct))
                        {
                            if(count($ct) == 0)
                            {
                                $data[] = array('result' => 'No results');
                            }
                            else
                            {
                                $data[] = array(
                                    'result' => 'OK',
                                    'type' => $type,
                                    'items' => $ct);
                            }
                        }
                    }
                }
            }
            else
            {
                $data[] = Array('result' => 'Incorrect URL.');
            }
        }
        else
        {
            $data[] = Array('result' => 'No date specified');
        }
        return new JsonResponse($data);
    }

    /**
     * Show extra page of content types from homepage
     *
     * @Template()
     * @param Request $request
     * @return array | Response
     */
    public function extraAction(Request $request)
    {
        $status = [];
        $dateStart = $request->get('sd');
        $dateStop = $request->get('ed');
        $request->getPathInfo();
        $ctype = $request->get('ctype');

        ($ctype == 'contenttype') ? $typenaam = 'Content Types' : $typenaam = $ctype;
        if(($dateStart && $dateStop) != null)
        {
            if($ctype == 'contenttype')
            {
                if(($this->validateDate($dateStart)) && ($this->validateDate($dateStop)))
                {
                    $ctypes = $this->getContentTypes($dateStart, $dateStop, true, $ctype);
                    $placeholder = $this->getPaginator($ctypes);
                    $status = $placeholder['status'];
                    $paginator = $placeholder['paginator'];

                    return array(
                        'pager' => $paginator ,
                        'ctypes' => $ctypes,
                        'taxitems' => [],
                        'status' => $status,
                        'typenaam' => $typenaam,
                         'slug' => 'Overige'
                    );
                }
                else
                {
                    $status[] = Array('result' => 'Dateformat is not valid.');
                }
            }
            else
            {
                $status[] = Array('result' => 'Incorrect URL.');
            }
        }
        else
        {
            $status[] = Array('result' => 'No date specified.');
        }
        return array(
            'ctypes' => [],
            'taxitems' => [],
            'status' => $status,
            'typenaam' => $typenaam,
            'slug' => 'Overige'
        );
    }

    /**
     * Show extra page of content types from detail page or
     * show extra page of a taxonomy type from homepage
     *
     * @Template()
     * @param Request $request
     * @return array | Response
     */
    public function detailextraAction(Request $request)
    {
        $status = [];
        $dateStart = $request->get('sd');
        $dateStop = $request->get('ed');
        $request->getPathInfo();
        $type = $request->get('ctype');
        ($type == ('contenttype' || 'taxonomy') ? $typenaam = $request->get('typenaam') : $typenaam = $type);
        if(($dateStart && $dateStop) != null)
        {
            if($type == 'contenttype' || $type == 'taxonomy')
            {
                if(($this->validateDate($dateStart)) && ($this->validateDate($dateStop)))
                {
                    if($type == 'contenttype')
                    {
                        $taxtype = $request->get('tax');
                        $ct = $this->getTaxItems($dateStart, $dateStop, $typenaam, true);
                        if(array_key_exists($taxtype, $ct))
                        {
                            $ctypes = $ct[$taxtype]['items'];
                            if(count($ctypes) > 0)
                            {
                                $placeholder = $this->getPaginator($ctypes);
                                $status = $placeholder['status'];
                                $paginator = $placeholder['paginator'];

                                return array(
                                    'pager' => $paginator ,
                                    'ctypes' => $ctypes,
                                    'taxitems' => [],
                                    'type' => $taxtype,
                                    'status' => $status,
                                    'typenaam' => $typenaam,
                                    'slug' => 'Overige'
                                );
                            }
                            else
                            {
                                $status[] = Array(
                                    'result' => "No results were found."
                                );
                            }
                        }
                        else
                        {
                            $status[] = Array(
                                'result' => "No results were found for this taxonomy type."
                            );
                        }
                    }
                    elseif($type == 'taxonomy')
                    {
                        $taxitems = $this->getTaxItemsByType($dateStart, $dateStop, $typenaam);
                        if(count($taxitems) > 0)
                        {
                            $placeholder = $this->getPaginator($taxitems[$typenaam]['items']);
                            $status = $placeholder['status'];
                            $paginator = $placeholder['paginator'];

                            return array(
                                'pager' => $paginator ,
                                'ctypes' => [],
                                'taxitems' => $taxitems,
                                'status' => $status,
                                'typenaam' => str_replace('facet_','',$typenaam),
                                'slug' => 'Overige'
                            );
                        }
                        else
                        {
                            $status[] = Array('result' => "No results were found.");
                        }
                    }
                }
                else
                {
                    $status[] = Array('result' => 'Dateformat is not valid.');
                }
            }
            else
            {
                $status[] = Array('result' => 'Incorrect URL.');
            }
        }
        else
        {
            $status[] = Array('result' => 'No date specified.');
        }
        return array(
            'ctypes' => [],
            'taxitems' => [],
            'status' => $status,
            'typenaam' => str_replace('facet_','',$typenaam),
            'slug' => 'Overige'
        );
    }

    /**
     * Show extra page of content types from detail page of a taxonomy item
     *
     * @Template()
     * @param Request $request
     * @return array | Response
     */
    public function taxdetailextraAction(Request $request)
    {
        $status = [];
        $dateStart = $request->get('sd');
        $dateStop = $request->get('ed');
        $request->getPathInfo();
        $taxtype = $request->get('taxonomyType');
        $taxitem = $request->get('taxonomyItem');

        if(($dateStart && $dateStop) != null)
        {
            if(($this->validateDate($dateStart)) && ($this->validateDate($dateStop)))
            {
                $items = $this->getContentTypeByName($dateStart, $dateStop, $taxtype, $taxitem, true);
                if(count($items) > 0)
                {
                    $placeholder = $this->getPaginator($items);
                    $paginator = $placeholder['paginator'];
                    $status = $placeholder['status'];

                    return array(
                        'pager' => $paginator ,
                        'items' => $items,
                        'status' => $status,
                        'type' => $taxtype,
                        'typenaam' => str_replace('facet_','',$taxtype),
                        'itemnaam' => $taxitem,
                        'slug' => 'Overige'
                    );
                }
                else
                {
                    $status[] = Array(
                        'result' => "No results were found.");
                }
            }
            else
            {
                $status[] = Array(
                    'result' => 'Dateformat is not valid.');
            }
        }
        else
        {
            $status[] = Array(
                'result' => 'No date specified.');
        }

        return array(
            'items' => [],
            'status' => $status,
            'typenaam' => str_replace('facet_','',$taxtype),
            'itemnaam' => $taxitem,
            'slug' => 'Overige'
        );
    }

    /**
     * Return array of contentTypes based on given parameters
     *
     * @param rangeStart
     * @param rangeEnd
     * @param boolean $extra (optional)
     * @param string $ctype (optional)
     * @return array
     */
    function getContentTypes($rangeStart, $rangeEnd, $extra = false, $ctype = null)
    {
        /**
         * @var $client \Solarium\Client
         */
        $client = $this->get('solarium.client');

        $query = $client->createSelect();

        // Add filter for date range AND content type 'Article'.
//        $querystring = $helper->rangeQuery('creation_date', $rangeStart, $rangeEnd).'AND type_class:'.$this->articleClass;
        $querystring = 'creation_date: ["'.$rangeStart.'" TO "'.$rangeEnd.'"] AND type_class:'.$this->articleClass;
        $query->setQuery($querystring);
        $facetSet = $query->getFacetSet();
        $facetSet->createFacetField('contenttypes')->setField('type_name');

        // Execute the query
        $result = $client->select($query);

        // Populate graph data arrays
        $total = $result->getNumFound();
//        echo 'tott '.$total;
//        die();

        $data = [];
        if($total > 0)
        {
            $facets = $result->getFacetSet()->getFacets();
            $ctypes_rest = array(
                    'perc' => 0,
                    'title' => 'Overige',
                    'count' => 0
            );
            $teller = 0;
            foreach($facets as $facet)
            {
                foreach($facet as $title => $count)
                {
                    if($count > 0)
                    {
                        $perc = $this->getPerc($count, $total);

                        if(!$extra)
                        {
                            if($teller <= 9)
                            {
                                $data[] = Array(
                                    'perc' => $perc,
                                    'title' => $title,
                                    'count' => $count
                                );
                            }
                            else if($teller > 9)
                            {
                                $ctypes_rest['perc'] += $perc;
                                $ctypes_rest['count'] += $count;
                            }
                            $teller++;
                        }
                        else
                        {
                            $data[] = Array(
                                'perc' => $perc,
                                'title' => $title,
                                'count' => $count,
                                'type' => $ctype
                            );
                        }
                    }
                }
                if($ctypes_rest['perc'] != '0')
                {
                    $data[] = $ctypes_rest;
                }
            }
        }
        return $data;
    }

    // TODO: Remove
    function clrSolr()
    {
        /** @var $client \Solarium\Client */
        $client = $this->get('solarium.client');
        // get an update query instance
        $update = $client->createUpdate();

        // add the delete query and a commit command to the update query
        $update->addDeleteQuery('*:*');
        $update->addCommit();

        // this executes the query and returns the result
        $result = $client->update($update);

        echo 'SOLR was cleared!<br/>';
        echo 'Query status: ' . $result->getStatus(). '<br/>';
        echo 'Query time: ' . $result->getQueryTime();
        die();
    }

    /**
     * Return array of taxonomy items based on given parameters
     *
     * @param $rangeStart
     * @param $rangeEnd
     * @param string $contentType (optional)
     * @param boolean $extra (optional)
     * @return array
     */
    function getTaxItems($rangeStart, $rangeEnd, $contentType = null, $extra = false)
    {
        /** @var $client \Solarium\Client */
        $client = $this->get('solarium.client');

        // get a select query instance
        $query = $client->createSelect();
        $type = '';
        if($contentType)
        {
            $type = 'AND type_name: "'.$contentType.'"';
        }
        $querystring = 'creation_date: ["'.$rangeStart.'" TO "'.$rangeEnd.'"] AND type_class:'.$this->articleClass.' '.$type;
        $query->setQuery($querystring);
        // $helper->rangeQuery('_time_', $rangeStart, $rangeEnd).'AND type_class: '.$this->articleClass.' '.$type

        $taxis = array('facet_keyword', 'facet_news_category');
        $items = [];
        foreach($taxis as $tax)
        {
            $facetSet = $query->getFacetSet();
            $facetSet->createFacetField($tax.'s')->setField($tax);
            $resultset = $client->select($query);
            $total = 0;
            $keys = $resultset->getFacetSet()->getFacet($tax.'s');

            $overigecounter = 0;
            if($resultset->getNumFound() > 0)
            {
                $restitems = array('perc' => 0,
                                   'title' => 'Overige',
                                   'count' => 0);
                foreach($keys as $iets => $value)
                {
                    $total += $value;
                }
                if($total > 0)
                {
                    $items[$tax]['items'] = [];
                    $items[$tax]['totalcount'] = $total;
                    foreach($keys as $key => $value)
                    {
                        if($value > 0)
                        {
                            if($extra)
                            {
                                $items[$tax]['items'][] = array(
                                    'perc' => $this->getPerc($value, $total),
                                    'title' => $key,
                                    'count' => $value);
                            }
                            else
                            {
                                if($overigecounter <= 9)
                                {
                                    $items[$tax]['items'][] = array(
                                        'perc' => $this->getPerc($value, $total),
                                        'title' => $key,
                                        'count' => $value);
                                    $overigecounter++;
                                }
                                else
                                {
                                    $restitems['perc'] += $this->getPerc($value, $total);
                                    $restitems['count'] += $value;
                                }
                            }
                        }
                    }
                    if($overigecounter >= 9)
                    {
                        $items[$tax]['items'][] = $restitems;
                    }
                }
            }
        }
        return $items;
    }

    /**
     * Return array of taxonomy items for a specific type
     *
     * @param $rangeStart
     * @param $rangeEnd
     * @param string $contentItem
     * @return array
     */
    function getTaxItemsByType($rangeStart, $rangeEnd, $contentItem)
    {
        /** @var $client \Solarium\Client */
        $client = $this->get('solarium.client');

        // get a select query instance
        $query = $client->createSelect();
        $querystring = 'creation_date: ["'.$rangeStart.'" TO "'.$rangeEnd.'"] AND type_class:'.$this->articleClass;
        $query->setQuery($querystring);

        // create array of relationTypes. Currently hardcoded.
        $taxis = array('facet_keyword');
        $items = [];
        foreach($taxis as $tax)
        {
            $facetSet = $query->getFacetSet();
            $facetSet->createFacetField($tax)->setField($tax);

            $resultset = $client->select($query);
            $total = 0;
            $keys = $resultset->getFacetSet()->getFacet($tax);
            if($resultset->getNumFound() > 0)
            {
                $items[$tax]['items'] = [];

                // Calculate total before creating items.
                foreach($keys as $key => $value)
                {
                    $total += $value;
                }
                // Add items to array if they match the contentItem
                foreach($keys as $key => $value)
                {
                    if($tax == $contentItem)
                    {
                        if($value > 0)
                        {
                            $items[$tax]['items'][] = array(
                                'perc' => $this->getPerc($value, $total),
                                'title' => $key,
                                'count' => $value,
                                'type' => $contentItem);
                        }
                    }
                }
                // add the totalcount, to be used for calculating percentage later
                $items[$tax]['totalcount'] = $total;
            }
        }
        return $items;
    }

    /**
     * Return the contentTypes that are linked to taxonomy item of type $type
     *
     * @param $rangeStart
     * @param $rangeEnd
     * @param $type
     * @param $item
     * @param boolean $extra (optional)
     * @return array
     */
    function getContentTypeByName($rangeStart, $rangeEnd, $type, $item, $extra = false)
    {
        /** @var $client \Solarium\Client */
        $client = $this->get('solarium.client');

        // get a select query instance
        $query = $client->createSelect();
        $querystring = 'creation_date: ["'.$rangeStart.'" TO "'.$rangeEnd.'"] AND type_class:'.$this->articleClass.' AND '.$type.': ["'.$item.'" TO "'.$item.'"]';
        $query->setQuery($querystring);

        $items = [];
        $facetSet = $query->getFacetSet();
        $facetSet->createFacetField($type)->setField('type_name');
        $resultset = $client->select($query);

        $keys = $resultset->getFacetSet()->getFacet($type);
        $overigecounter = 0;
        $total = 0;
        $restitems = array('perc' => 0,
            'title' => 'Overige',
            'count' => 0);

        if($resultset->getNumFound() > 0)
        {
            foreach($keys as $iets => $value)
            {
                $total += $value;
            }
            foreach($keys as $key => $value)
            {
                if($value > 0)
                {
                    if($extra)
                    {
                        $items[] = Array(
                            'perc' => $this->getPerc($value, $total),
                            'title' => $key,
                            'count' => $value);
                    }
                    else
                    {
                        if($overigecounter <= 9)
                        {
                            $items[] = Array(
                                'perc' => $this->getPerc($value, $total),
                                'title' => $key,
                                'count' => $value);
                            $overigecounter++;
                        }
                        else
                        {
                            $restitems['perc'] += $this->getPerc($value, $total);
                            $restitems['count'] += $value;
                        }
                    }
                }
            }
        }
        if($overigecounter > 9)
        {
            $items[] = $restitems;
        }
        return $items;
    }

    /**
     * Return true if $date is a valid ISODate
     *
     * @param $date
     * @return boolean
     */
    function validateDate($date)
    {
        if (preg_match('/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}).(\d{3})Z$/', $date, $parts) == true) {
            $time = gmmktime($parts[4], $parts[5], $parts[6], $parts[2], $parts[3], $parts[1]);
            $input_time = strtotime($date);

            if ($input_time === false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }

    /**
     * Calculate the percentage of $count against $total
     *
     * @param $count
     * @param $total
     * @return int
     */
    function getPerc($count, $total)
    {
        if($total > 0 && $count > 0)
        {
            $perc = $count/$total*100;
            $perc = number_format((float)$perc, 1, '.', '');
            if($perc == 0.0){$perc = 0.1;}
        }
        else
        {
            $perc = 0;
        }
        return $perc;
    }

    /**
     * Return array of items sorted in the knp_paginator
     *
     * @param $items
     * @return array
     */
    function getPaginator($items)
    {
        $status = [];
        if(sizeof($items) == 1)
        {
            $status[] = Array(
                'result' => sizeof($items)." result was found.");
        }
        elseif(sizeof($items) >= 1)
        {
            $status[] = Array(
                'result' => sizeof($items)." results were found.");
        }
        elseif(sizeof($items) <= 0)
        {
            $status[] = Array(
                'result' => "No results were found.");
        }

        /** @var $paginator \Knp\Component\Pager\Paginator */
        $paginator = $this->get('knp_paginator');
        $paginator = $paginator->paginate(
            $items,
            $this->get('request')->query->get('page', 1)/*page number*/,
            10/*limit per page*/
        );

        return array(
            'paginator' => $paginator,
            'status' => $status
        );
    }
}